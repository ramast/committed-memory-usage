#!/bin/bash
ps --no-headers -eo pid,cmd|while read line
do
   pid=`echo $line|sed 's:^ +::'|grep -oE '^[0-9]+'`
   size=`pmap $pid|tail -1|sed 's: total *::'`
   echo "$size $line"
done|sort -n
