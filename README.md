# Committed Memory Usage



## Introduction

This is a simple script that attempt to find process that consume highest amount of memory.
The difference between this script and all other available tools is that it checks memory the kernel has committed to the process not the amount of memory the process actually uses. This is especially useful when kernel is configured to not overcommit memory (or not overcommit too much).

To run the script simply download it and execute it `bash get_committed_memory.sh` as a root.

Processes are ordered by committed memory. Last process has the largest amount of committed memory.

## Sources / Inspiration

The script was based on this staskoverflow question/answer https://serverfault.com/questions/446318/how-can-i-find-out-the-amount-of-memory-commited-by-a-process-on-linux
